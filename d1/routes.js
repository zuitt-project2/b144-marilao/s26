const http = require('http');

// create a variable 'port' to store the port number
const port = 4000

// create a var 'server' that stores the output of the 'create server' method
const server = http.createServer((req, res) => {

	// http://localhost:4000/greeting
	if(req.url == '/greeting'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This is the greeting')
	}

	// access the /homepage routes returns a message of 'This is the homepage ' with a 200 status
	if(req.url == '/homepage'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This is the homepage')
	}

	else{res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page Not Found')

	}
})

// Use the 'server' and 'port' variables created above
server.listen(port);

// When server is running, console will print the message:
console.log(`Server now accesible at localhost: ${port}`)

