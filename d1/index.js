// Use the require directive to load Node.js Modules
// "module" is a software component or part of a program that contains one r more routines
// 'http module'- lets Node.js transfer data using the Hypertext transfer protocol. It is a set of inidividual files that contain code to create a "component" that helps data transfer between apps

let http = require("http");
// Clients(browser) and servers(nodeJs/express js app) communicate by exchanging individual messages.
// message sent by the client, usually a web browser are called requests
// sample url - (http://home)
// message sent by the server as an answer are called responses.

// createServer() method - used to create an HTTP server that listens to requests on a specified port and gives responses back to the client
// it accepts a function and allows us to perform a certain task for our server
http.createServer(function(request, response) {

	// Use the writeHead()method to:
	// set a status code for the response-200 means OK(successful)
	// Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// We used the response.end() method to end the response process
	response.end('Goodbye')

}).listen(4000) //port in a virtual point where network connections start and end
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the 'listen(4000)' method where the server will listen to any requests that are sent to our server

// when a server is running, console will print this message:
console.log('Server running at localhost:4000')

// default: http://localhost:4000

